import React, {useState, useEffect} from "react";
import Masonry from 'react-masonry-component';
import ModalImage from "react-modal-image";

import '../styles/layout/_gallery.scss';


function Gallery(){
  useEffect(() => {
    fetchGallery();
  },[]);

  const [gallery, setGallery] = useState([]);

  const fetchGallery = async () => {
    const data = await fetch('https://api.unsplash.com/search/photos?page=1&per_page=10&query=sea&client_id=b1236cf0c7cc9bc82060984efc4b60992e3abec2f584678d351a19730f1d1b93');

    if(data.status === 200) {
      const gallery = await data.json();
      setGallery(gallery.results);
    } else {
      console.log(data.status);
    }
  }

  const trimDate = (date) => {
    let createdDate = date.substring(0, 10);
    return createdDate;
  }

  return(
    <div className="gallery">
      <Masonry>
        {gallery.map((item) => (
          <div className="gallery__item" key={item.id}>
            <ModalImage
              className="gallery__image"
              showRotate={true}
              small={item.urls.regular}
              large={item.urls.full}
              alt={ item.description ? <h1>{item.description}</h1> : <h1>{item.alt_description}</h1> }
            />
            <div className="gallery__image--info">
              { item.description ? <h1>{item.description}</h1> : <h1>{item.alt_description}</h1> }
              <p>{trimDate(item.created_at)}</p>
            </div>
          </div>
        ))}
      </Masonry>
    </div>
  );
}

export default Gallery;
